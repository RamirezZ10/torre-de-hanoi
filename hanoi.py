import heapq
import pygame
import time
import sys
import os

# Clase Nodo para representar cada estado en la búsqueda A*
class Nodo:
    def __init__(self, estado, costo_g, costo_h, padre = None):
        self.estado = estado
        self.costo_g = costo_g
        self.costo_h = costo_h
        self.padre = padre

    def __lt__(self, other):
        return self.costo_g + self.costo_h < other.costo_g + other.costo_h

    def __eq__(self, other):
        return self.estado == other.estado

# Función para encontrar el camino óptimo utilizando A*
def buscar_camino_optimo(inicio, objetivo):
    lista_candidatos = []
    nodos_explorados = []
    heapq.heappush(lista_candidatos, Nodo(inicio, 0, distancia_manhattan(inicio, objetivo)))

    while lista_candidatos:
        nodo_actual = heapq.heappop(lista_candidatos)
        if nodo_actual.estado == objetivo:
            camino_optimo = []
            while nodo_actual:
                camino_optimo.append(nodo_actual.estado)
                nodo_actual = nodo_actual.padre
            return camino_optimo[::-1]
        
        nodos_explorados.insert(0, nodo_actual)
        
        for siguiente_estado in generar_estados_siguientes(nodo_actual.estado):
            costo_g = nodo_actual.costo_g + 1
            costo_h = distancia_manhattan(siguiente_estado, objetivo)
            nodo_siguiente = Nodo(siguiente_estado, costo_g, costo_h, nodo_actual)
            
            if nodo_siguiente in nodos_explorados:
                continue
                
            if nodo_siguiente not in lista_candidatos:
                heapq.heappush(lista_candidatos, nodo_siguiente)
            else:
                for nodo in lista_candidatos:
                    if nodo == nodo_siguiente and nodo.costo_g > nodo_siguiente.costo_g:
                        lista_candidatos.remove(nodo)
                        heapq.heappush(lista_candidatos, nodo_siguiente)
                        break

# Función para generar todos los posibles estados siguientes
def generar_estados_siguientes(estado):
    estados_siguientes = []
    x = 0

    #( [3, 2, 1], [], [] )
    #( [3, 2], [], [1] )
    #( [3], [2], [1] )

    for i in range(len(estado)):
        for j in range(len(estado)):
            if i != j:
                if not estado[i]:
                    nuevo_estado = list(estado)
                    nuevo_estado[j] = nuevo_estado[j][:-1]
                    nuevo_estado[i] = nuevo_estado[i] + estado[j][-1:]
                    estados_siguientes.append(tuple(nuevo_estado))

                elif not estado[j]:
                    nuevo_estado = list(estado)
                    nuevo_estado[i] = nuevo_estado[i][:-1]
                    nuevo_estado[j] = nuevo_estado[j] + estado[i][-1:]
                    estados_siguientes.append(tuple(nuevo_estado))

                elif estado[i][-1] < estado[j][-1]:
                    nuevo_estado = list(estado)
                    nuevo_estado[i] = nuevo_estado[i][:-1]
                    nuevo_estado[j] = nuevo_estado[j] + estado[i][-1:]
                    estados_siguientes.append(tuple(nuevo_estado))

                elif estado[j][-1] < estado[i][-1]:
                    nuevo_estado = list(estado)
                    nuevo_estado[j] = nuevo_estado[j][:-1]
                    nuevo_estado[i] = nuevo_estado[i] + estado[j][-1:]
                    estados_siguientes.append(tuple(nuevo_estado))

    #print(f"ESTADOS SIGUIENTES: {estados_siguientes}")
    return estados_siguientes

# Función para calcular la distancia de Manhattan entre dos estados
def distancia_manhattan(estado1, estado2):
    distancia = 0
    for i in range(len(estado1)):
        for j in range(len(estado1[i])):
            if estado1[i][j]:
                disco = estado1[i][j]
                for k in range(len(estado2)):
                    for l in range(len(estado2[k])):
                        if estado2[k][l] == disco:
                            distancia += abs(i-k) + abs(j-l)

    return distancia

def visualizar_proceso(camino_optimo):
    ANCHO_VENTANA = 1200
    ALTO_VENTANA = 480
    COLOR_FONDO = (0, 0, 0)
    COLOR_TORRES = (255, 255, 255)
    COLOR_DISCOS = (255, 0, 0)
    TAMAÑO_DISCO_BASE = 100
    ALTURA_DISCO = 20
    ESPACIO_ENTRE_TORRES = 250
    ESPACIO_ENTRE_DISCOS = 40

    # Inicializar Pygame
    pygame.init()
    ventana = pygame.display.set_mode((ANCHO_VENTANA, ALTO_VENTANA))
    pygame.display.set_caption('Torres de Hanoi')

    # Dibujar las torres
    x_torre_izquierda = ANCHO_VENTANA / 2 - TAMAÑO_DISCO_BASE - ESPACIO_ENTRE_TORRES
    x_torre_central = ANCHO_VENTANA / 2
    x_torre_derecha = ANCHO_VENTANA / 2 + TAMAÑO_DISCO_BASE + ESPACIO_ENTRE_TORRES
    y_base_torre = ALTO_VENTANA - 100
    y_top_torre = ALTO_VENTANA - 400
    pygame.draw.rect(ventana, COLOR_TORRES, (x_torre_izquierda, y_top_torre, TAMAÑO_DISCO_BASE, y_base_torre - y_top_torre))
    pygame.draw.rect(ventana, COLOR_TORRES, (x_torre_central, y_top_torre, TAMAÑO_DISCO_BASE, y_base_torre - y_top_torre))
    pygame.draw.rect(ventana, COLOR_TORRES, (x_torre_derecha, y_top_torre, TAMAÑO_DISCO_BASE, y_base_torre - y_top_torre))

    # Dibujar los discos en la posición inicial
    discos = []
    for i, disco in enumerate(camino_optimo[0][0]):
        tamaño_disco = TAMAÑO_DISCO_BASE - i * ESPACIO_ENTRE_DISCOS
        x = x_torre_izquierda + TAMAÑO_DISCO_BASE / 2 - tamaño_disco / 2
        y = y_base_torre - ALTURA_DISCO * (i + 1)
        #pygame.draw.rect(ventana, COLOR_DISCOS, (x, y, tamaño_disco, ALTURA_DISCO))
        #discos.append((x, y, tamaño_disco))

    # Actualizar la ventana
    pygame.display.flip()

    def mover_disco():
        for i in camino_optimo:
            torre = 0
            for j in i:
                altura = 0
                for disco in j:
                    tamaño_disco = TAMAÑO_DISCO_BASE + disco * ESPACIO_ENTRE_DISCOS

                    if torre == 0:
                        x = x_torre_izquierda + TAMAÑO_DISCO_BASE / 2 - tamaño_disco / 2

                    elif torre == 1:
                        x = x_torre_central + TAMAÑO_DISCO_BASE / 2 - tamaño_disco / 2

                    elif torre == 2:
                        x = x_torre_derecha + TAMAÑO_DISCO_BASE / 2 - tamaño_disco / 2

                    y = y_base_torre - ALTURA_DISCO * (altura + 1)
                    altura = altura + 1
                    pygame.draw.rect(ventana, COLOR_DISCOS, (x, y, tamaño_disco, ALTURA_DISCO))
                    pygame.display.flip()

                torre = torre + 1

            ventana.fill(COLOR_FONDO)
            pygame.draw.rect(ventana, COLOR_TORRES, (x_torre_izquierda, y_top_torre, TAMAÑO_DISCO_BASE, y_base_torre - y_top_torre))
            pygame.draw.rect(ventana, COLOR_TORRES, (x_torre_central, y_top_torre, TAMAÑO_DISCO_BASE, y_base_torre - y_top_torre))
            pygame.draw.rect(ventana, COLOR_TORRES, (x_torre_derecha, y_top_torre, TAMAÑO_DISCO_BASE, y_base_torre - y_top_torre))
            #time.sleep(2)
            a = input()

    mover_disco()

    corriendo = True
    while corriendo:
        for evento in pygame.event.get():
            if evento.type == pygame.QUIT:
                corriendo = False
    pygame.quit()

if __name__ == "__main__":

    os.system("clear")
    #------------------------------------------------------------------------------------------
    try:
        n_discos = int(sys.argv[1]) #AQUI SE INSERTA EL NUMERO DE DISCOS
    except Exception as e:
        print("\nERROR!!! \nMODO DE USO: python3 hanoi.py [numero de discos]")
        exit()
    #------------------------------------------------------------------------------------------

    lista = []
    for i in range(1, n_discos + 1):
        lista.insert(0, i)

    inicio = ( lista, [], [] )
    objetivo = ( [], [], lista )
        
    camino_optimo = buscar_camino_optimo(inicio, objetivo)
    #print(f"\n\nCAMINO: {camino_optimo}")
    #print(f"\n\nMovimientos minimos: {len(camino_optimo) - 1}")

    visualizar_proceso(camino_optimo)
